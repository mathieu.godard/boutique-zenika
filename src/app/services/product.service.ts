import { Injectable } from '@angular/core';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products: Product[] = [];
  stock: number = 0;
  minimumPrice: number = 20;

  constructor() {}

  getProducts(): Product[] {
    this.products = [
      new Product(
        'Men Sweatshirt',
        'C0D1NG_TH3_W0RLD BIO HOODIE - MEN',
        'https://s3.eu-central-1.amazonaws.com/balibart-s3/Products/5acf344514006a7fe670e2eb/Mockups/front.png',
        39,
        10
      ),
      new Product(
        'Men T-Shirt',
        'BIO T-SHIRT WITH CREWNECK - MEN.',
        'https://s3.eu-central-1.amazonaws.com/balibart-s3/Products/5b2911e4ab33424aec592bd6/Mockups/front.png',
        19,
        20
      ),
      new Product(
        'T-Shirt women',
        'BIO T-SHIRT WITH CREWNECK - WOMEN.',
        'https://s3.eu-central-1.amazonaws.com/balibart-s3/Products/5b290d26ab33424aec592bd4/Mockups/front.png',
        30,
        14
      ),
      new Product(
        'Tote bag',
        'C0D1NG_TH3_W0RLD, BIO TOTE BAG.',
        'https://s3.eu-central-1.amazonaws.com/balibart-s3/Products/5acf160814006a7fe670e2dd/Mockups/front.png',
        12.5,
        1
      ),
    ];
    return this.products;
  }

  isLast(product: Product): boolean {
    return product.stock === 1;
  }

  isAvailable(product: Product): boolean {
    return product.stock > 0;
  }

  decreaseStock(product: Product): void {
    product.stock--;
  }
}
