import { Component } from '@angular/core';
import { Product } from './model/product';
import { CustomerService } from './services/customer.service';
import { ProductService } from './services/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  products: Product[] = [];

  constructor(
    public productService: ProductService,
    public customerService: CustomerService
    ) {
    this.productService.getProducts();
  }

  addProductToBasketAndDecreaseStock(product: Product) {
    this.productService.decreaseStock(product);
    this.customerService.addProduct(product);
  }
}
