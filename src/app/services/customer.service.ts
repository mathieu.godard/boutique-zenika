import { Injectable } from '@angular/core';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  basket: Product[] = [];

  constructor() {}

  addProduct(product: Product): void {
    this.basket.push(product);
  }

  get total(): number {
    return this.basket.reduce((acc, val) => (acc += val.price), 0);
  }
}
