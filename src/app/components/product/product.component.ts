import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent {
  @Input()
  product?: Product;

  @Output()
  addToBasket: EventEmitter<Product> = new EventEmitter();

  stock: number = 0;

  constructor(public productService: ProductService) {}

  clickButton(): void {
    this.addToBasket.emit(this.product);
  }

}
